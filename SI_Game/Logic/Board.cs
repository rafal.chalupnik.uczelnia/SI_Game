﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic
{
    public class Board : ICloneable
    {
        private readonly char[,] board_;
        private bool isPlayerOneTurn_ = true;
        private int overallMovesTaken_;
        private readonly int[] playersPoints_;

        private bool CheckCoordinates(int _row, int _column)
        {
            return _row >= 0 
                && _row < Size 
                && _column >= 0 
                && _column < Size;
        }
        private int CountPointsInColumn(int _column)
        {
            for (var i = 0; i < Size; i++)
            {
                if (board_[i, _column] == ' ')
                    return 0;
            }
            return Size;
        }
        private int CountPointsInRow(int _row)
        {
            for (var i = 0; i < Size; i++)
            {
                if (board_[_row, i] == ' ')
                    return 0;
            }
            return Size;
        }
        private int CountPointsOnDiagonals(int _row, int _column)
        {
            int CalculateDiagonal(bool _horizontalRight, bool _verticalUp)
            {
                var row = _row;
                var col = _column;
                var counter = 0;
                var isDiagonalFull = true;

                while (CheckCoordinates(row, col) && isDiagonalFull)
                {
                    if (board_[row, col] == ' ')
                        isDiagonalFull = false;
                    else
                    {
                        counter++;
                        row += _verticalUp ? 1 : -1;
                        col += _horizontalRight ? 1 : -1;
                    }
                }

                return isDiagonalFull ? counter : 0;
            }

            var leftDiagonal = CalculateDiagonal(false, false);
            if (leftDiagonal > 0)
            {
                var secondPart = CalculateDiagonal(true, true);
                if (secondPart > 0)
                    leftDiagonal += secondPart - 1;
                else leftDiagonal = 0;
            }

            var rightDiagonal = CalculateDiagonal(true, false);
            if (rightDiagonal > 0)
            {
                var secondPart = CalculateDiagonal(false, true);
                if (secondPart > 0)
                    rightDiagonal += secondPart - 1;
                else rightDiagonal = 0;
            }

            if (leftDiagonal < 2)
                leftDiagonal = 0;

            if (rightDiagonal < 2)
                rightDiagonal = 0;

            return leftDiagonal + rightDiagonal;
        }
        private int EvaluateMove(int _row, int _column)
        {
            if (board_[_row, _column] == 'X')
                return 0;

            board_[_row, _column] = 'X';

            var points = CountPointsInRow(_row) 
                + CountPointsInColumn(_column) 
                + CountPointsOnDiagonals(_row, _column);
            
            board_[_row, _column] = ' ';

            return points;
        }

        public char this[int _row, int _column] => CheckCoordinates(_row, _column)
            ? board_[_row, _column]
            : ' ';
        public int ActualPlayer => isPlayerOneTurn_ ? 0 : 1;
        public bool IsGameOver => overallMovesTaken_ == SquaredSize;
        public int MovesMade => overallMovesTaken_;
        public IReadOnlyList<int> Scores => playersPoints_.ToList().AsReadOnly();
        public int Size { get; }
        public int SquaredSize { get; }

        public Board(int _size)
        {
            if (_size <= 1)
                throw new ArgumentException("Rozmiar musi być równy przynajmniej 2!");
            
            Size = _size;
            SquaredSize = Size * Size;

            board_ = new char[Size, Size];
            playersPoints_ = new int[2];

            for (var i = 0; i < SquaredSize; i++)
                board_[i / Size, i % Size] = ' ';
        }

        public object Clone()
        {
            var output = new Board(Size);
            
            for (var r = 0; r < Size; r++)
            {
                for (var c = 0; c < Size; c++)
                    output.board_[r, c] = board_[r, c];
            }

            output.isPlayerOneTurn_ = isPlayerOneTurn_;
            output.overallMovesTaken_ = overallMovesTaken_;

            for (var i = 0; i < playersPoints_.Length; i++)
                output.playersPoints_[i] = playersPoints_[i];

            return output;
        }
        public override bool Equals(object _object)
        {
            if (_object == null)
                return false;

            if (_object is Board board)
            {
                if (Size != board.Size)
                    return false;

                for (var r = 0; r < Size; r++)
                {
                    for (var c = 0; c < Size; c++)
                    {
                        if (board_[r, c] != board[r, c])
                            return false;
                    }
                }

                return true;
            }

            return false;
        }
        public int GetMovePoints(int _row, int _column)
        {
            if (!CheckCoordinates(_row, _column))
                return -1;

            return EvaluateMove(_row, _column);
        }
        public bool IsMoveValid(int _row, int _column)
        {
            return CheckCoordinates(_row, _column) 
                && board_[_row, _column] == ' ';
        }
        public bool Move(int _row, int _column)
        {
            if (!IsMoveValid(_row, _column)) return false;

            var points = EvaluateMove(_row, _column);
            board_[_row, _column] = 'X';

            playersPoints_[ActualPlayer] += points;
            isPlayerOneTurn_ = !isPlayerOneTurn_;

            overallMovesTaken_++;

            return true;
        }
        public override string ToString()
        {
            var separator = "-";
            for (var i = 0; i < Size; i++)
                separator += "----";

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(separator);

            for (var row = 0; row < Size; row++)
            {
                stringBuilder.Append("|");
                for (var col = 0; col < Size; col++)
                {
                    stringBuilder.Append(" ");
                    stringBuilder.Append(board_[row, col]);
                    stringBuilder.Append(" |");
                }
                stringBuilder.Append("\n");
                stringBuilder.AppendLine(separator);
            }

            stringBuilder.AppendLine();

            for (var p = 0; p < playersPoints_.Length; p++)
                stringBuilder.AppendLine($"Player {p + 1}: {playersPoints_[p]}");

            stringBuilder.AppendLine($"Actual player: {ActualPlayer + 1}");

            return stringBuilder.ToString();
        }
    }
}