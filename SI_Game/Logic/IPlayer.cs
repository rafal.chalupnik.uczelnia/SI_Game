﻿using System;

namespace Logic
{
    public interface IPlayer
    {
        Tuple<int, int> GetMove();
    }
}