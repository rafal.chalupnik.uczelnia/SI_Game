﻿using System;
using Logic.Algorithms;

namespace Logic
{

    public class ComputerPlayer<T> : IPlayer where T : class, IAlgorithm
    {
        private readonly T algorithm_;

        private ComputerPlayer(T _algorithm)
        {
            algorithm_ = _algorithm;
        }

        public static ComputerPlayer<T> Create(Board _board, bool _isPlayerOne = false, int _depth = 1)
        {
            var algorithm = Algorithm<T>.Create(_board, _isPlayerOne, _depth);

            return algorithm != null
                ? new ComputerPlayer<T>(algorithm)
                : null;
        }

        public Tuple<int, int> GetMove()
        {
            return algorithm_.GetMove();
        }
    }
}