﻿using System;
using System.Collections.Generic;
using Logic.Algorithms;

namespace Logic
{
    public enum Player
    {
        Human = 0,
        Random = 1,
        Greedy = 2,
        Minimax = 3,
        MinimaxLinq = 4,
        AlphaBeta = 5,
        AlphaBetaLinq = 6,
        Hybrid = 7
    }

    public class Game
    {
        private readonly Board board_;
        private readonly IPlayer[] players_;

        private IPlayer CreateComputerPlayer(Player _player, bool _isPlayerOne, int _depth = 1)
        {
            switch (_player)
            {
                case Player.Random:
                    return ComputerPlayer<RandomAlgorithm>.Create(board_, _isPlayerOne, _depth);
                case Player.Greedy:
                    return ComputerPlayer<GreedyAlgorithm>.Create(board_, _isPlayerOne, _depth);
                case Player.Minimax:
                    return ComputerPlayer<MinimaxAlgorithm>.Create(board_, _isPlayerOne, _depth);
                case Player.MinimaxLinq:
                    return ComputerPlayer<MinimaxLinqAlgorithm>.Create(board_, _isPlayerOne, _depth);
                case Player.AlphaBeta:
                    return ComputerPlayer<AlphaBetaAlgorithm>.Create(board_, _isPlayerOne, _depth);
                case Player.AlphaBetaLinq:
                    return ComputerPlayer<AlphaBetaLinqAlgorithm>.Create(board_, _isPlayerOne, _depth);
                case Player.Hybrid:
                    return ComputerPlayer<HybridAlgorithm>.Create(board_, _isPlayerOne, _depth);
                default:
                    return null;
            }
        }
        private void IncreaseActualPlayer()
        {
            ActualPlayer++;
            if (ActualPlayer == players_.Length)
                ActualPlayer = 0;
        }

        private Game(int _boardSize)
        {
            board_ = new Board(_boardSize);
            players_ = new IPlayer[2];
        }

        public static Game Create(int _boardSize)
        {
            return _boardSize >= 2
                ? new Game(_boardSize)
                : null;
        }

        public int ActualPlayer { get; private set; }
        public bool IsGameOver => board_.IsGameOver;
        public IReadOnlyList<int> Scores => board_.Scores;

        public Tuple<int, int> GetComputerMove()
        {
            var move = players_[ActualPlayer]?.GetMove();
            if (move != null)
            {
                IncreaseActualPlayer();
                board_.Move(move.Item1, move.Item2);
            }

            return move;
        }
        public bool HumanMove(Tuple<int, int> _move)
        {
            if (_move == null)
                return false;

            if (!board_.Move(_move.Item1, _move.Item2)) return false;

            IncreaseActualPlayer();
            return true;
        }
        public bool IsNowHumanMove()
        {
            return players_[ActualPlayer] == null;
        }
        public bool SetComputerPlayer(Player _player, int _playerIndex, int _depth)
        {
            if (_player == Player.Human || _playerIndex < 0 || _playerIndex > 1 || _depth < 1)
                return false;

            players_[_playerIndex] = CreateComputerPlayer(_player, _playerIndex == 0, _depth);
            return true;
        }
    }
}