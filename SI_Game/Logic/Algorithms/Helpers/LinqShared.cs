﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Logic.Algorithms.Helpers
{
    public class LinqShared
    {
        private const bool CustomThreading = true;
        //private const int ThreadCount = 30;

        public static IEnumerable<Tuple<int, int>> GetPossibleMoves(Board _board)
        {
            var boardSize = _board.Size;

            for (var r = 0; r < boardSize; r++)
            {
                for (var c = 0; c < boardSize; c++)
                {
                    if (_board.IsMoveValid(r, c))
                        yield return Tuple.Create(r, c);
                }
            }
        }

        public static Tuple<int, int> GetMove(Board _board, bool _isPlayerOne, Func<Tuple<int, int>, NodeLinq> _executeAlgorithm)
        {
            if (_board == null || _executeAlgorithm == null)
                return null;

            var threadCount = _board.SquaredSize;

            var firstLayerMoves = GetPossibleMoves(_board).ToList();
            IEnumerable<NodeLinq> firstLayerNodes = null;

            if (CustomThreading)
            {
                firstLayerNodes = new List<NodeLinq>();
                var threads = new List<Thread>();

                for (var i = 0; i < threadCount; i++)
                {
                    var thread = new Thread(() =>
                    {
                        Tuple<int, int> move;
                        while (firstLayerMoves.Count != 0)
                        {
                            lock (firstLayerMoves)
                            {
                                move = firstLayerMoves.FirstOrDefault();
                                if (firstLayerMoves.Count != 0)
                                    firstLayerMoves.RemoveAt(0);
                            }

                            if (move != null)
                            {
                                var result = _executeAlgorithm(move);
                                if (result != null)
                                {
                                    lock (firstLayerNodes)
                                    {
                                        (firstLayerNodes as List<NodeLinq>).Add(result);
                                    }
                                }
                            }
                        }
                    });
                    threads.Add(thread);
                    thread.Start();
                }

                foreach (var thread in threads)
                    thread.Join();
            }
            else
            {
                firstLayerNodes =
                    firstLayerMoves.Select(_executeAlgorithm)
                        .AsParallel();
            }

            Func<NodeLinq, NodeLinq, NodeLinq> nodeAggregation;
            firstLayerNodes = firstLayerNodes.Where(_node => _node != null);

            if (_isPlayerOne)
                nodeAggregation = (_node1, _node2) =>
                    _node1.HeuristicValue > _node2.HeuristicValue ? _node1 : _node2;
            else
                nodeAggregation = (_node1, _node2) =>
                    _node1.HeuristicValue < _node2.HeuristicValue ? _node1 : _node2;

            return firstLayerNodes.Aggregate(nodeAggregation).Move;
        }
    }
}