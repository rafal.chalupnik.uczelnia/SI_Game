﻿using System;

namespace Logic.Algorithms.Helpers
{
    public class NodeLinq
    {
        public int HeuristicValue { get; set; }
        public Tuple<int, int> Move { get; set; }
    }
}