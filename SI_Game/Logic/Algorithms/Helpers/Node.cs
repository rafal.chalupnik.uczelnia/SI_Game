﻿using System;
using System.Collections.Generic;

namespace Logic.Algorithms.Helpers
{
    public class Node
    {
        public List<Node> Children { get; }
        public int HeuristicValue { get; set; }
        public bool IsPlayerOne { get; set; }
        public Tuple<int, int> Move { get; }

        public Node(Tuple<int, int> _move = null)
        {
            Move = _move;

            Children = new List<Node>();
        }
    }
}