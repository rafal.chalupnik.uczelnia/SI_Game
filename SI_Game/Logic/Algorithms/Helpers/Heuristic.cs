﻿namespace Logic.Algorithms.Helpers
{
    public static class Heuristic
    {
        public static int GetBoardHeuristicValue(Board _board)
        {
            var scores = _board.Scores;
            var diff = scores[0] - scores[1];
            return diff;
            //return diff > 0 ? scores[0] : -scores[1];
        }
    }
}