﻿using System;

namespace Logic.Algorithms
{
    public class GreedyAlgorithm : Algorithm<GreedyAlgorithm>
    {
        private readonly Board board_;
        private readonly RandomAlgorithm randomAlgorithm_;

        private GreedyAlgorithm(Board _board, bool _isPlayerOne, int _depth)
        {
            board_ = _board;
            randomAlgorithm_ = Algorithm<RandomAlgorithm>.Create(_board);
        }

        public override Tuple<int, int> GetMove()
        {
            var maxPoints = 0;
            Tuple<int, int> move = null;

            for (var r = 1; r <= board_.Size; r++)
            {
                for (var c = 1; c <= board_.Size; c++)
                {
                    var points = board_.GetMovePoints(r, c);
                    if (points > maxPoints)
                    {
                        maxPoints = points;
                        move = Tuple.Create(r, c);
                    }
                }
            }

            if (maxPoints == 0)
                move = randomAlgorithm_.GetMove();
            else
                randomAlgorithm_.RemovePossibility(move);

            return move;
        }
    }
}