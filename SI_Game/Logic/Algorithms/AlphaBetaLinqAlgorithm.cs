﻿using System;
using Logic.Algorithms.Helpers;

namespace Logic.Algorithms
{
    public class AlphaBetaLinqAlgorithm : Algorithm<AlphaBetaLinqAlgorithm>
    {
        private static NodeLinq ExecuteAlphaBeta(Board _board, int _depth, bool _maximize, Tuple<int, int> _move = null, int _alpha = int.MinValue, int _beta = int.MaxValue)
        {
            _board = _board.Clone() as Board;
            _board.Move(_move.Item1, _move.Item2);

            if (_depth == 0 || _board.IsGameOver)
            {
                return new NodeLinq()
                {
                    HeuristicValue = Heuristic.GetBoardHeuristicValue(_board),
                    Move = _move
                };
            }

            var possibleMoves = LinqShared.GetPossibleMoves(_board);

            var returnNode = new NodeLinq()
            {
                Move = _move
            };

            if (_maximize)
            {
                // Maximize
                foreach (var possibleMove in possibleMoves)
                {
                    var childNode = ExecuteAlphaBeta(_board, _depth - 1, false, possibleMove, _alpha, _beta);
                    _alpha = Math.Max(_alpha, childNode.HeuristicValue);
                    returnNode.HeuristicValue = _alpha;

                    if (_alpha >= _beta)
                        break;
                }
            }
            else
            {
                // Minimize
                foreach (var possibleMove in possibleMoves)
                {
                    var childNode = ExecuteAlphaBeta(_board, _depth - 1, false, possibleMove, _alpha, _beta);
                    _beta = Math.Min(_beta, childNode.HeuristicValue);
                    returnNode.HeuristicValue = _beta;

                    if (_alpha >= _beta)
                        break;
                }
            }

            return returnNode;
        }

        private readonly Board board_;
        private readonly int depth_;
        private readonly bool isPlayerOne_;

        private AlphaBetaLinqAlgorithm(Board _board, bool _isPlayerOne, int _depth)
        {
            board_ = _board;
            depth_ = _depth;
            isPlayerOne_ = _isPlayerOne;
        }

        public override Tuple<int, int> GetMove()
        {
            return LinqShared.GetMove(board_, isPlayerOne_,
                _move => ExecuteAlphaBeta(board_, depth_, !isPlayerOne_, _move));
        }
    }
}