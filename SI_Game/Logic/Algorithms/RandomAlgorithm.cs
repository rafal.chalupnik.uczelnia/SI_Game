﻿using System;
using System.Collections.Generic;

namespace Logic.Algorithms
{
    public class RandomAlgorithm : Algorithm<RandomAlgorithm>
    {
        private readonly Random random_ = new Random();

        private readonly Board board_;
        private readonly List<Tuple<int, int>> possibilities_;

        private RandomAlgorithm(Board _board, bool _isPlayerOne, int _depth)
        {
            board_ = _board;
            possibilities_ = new List<Tuple<int, int>>();
            for (var r = 0; r < board_.Size; r++)
            {
                for (var c = 0; c < board_.Size; c++)
                    possibilities_.Add(Tuple.Create(r, c));
            }
        }
        
        public override Tuple<int, int> GetMove()
        {
            Tuple<int, int> move;

            do
            {
                move = possibilities_[random_.Next(possibilities_.Count)];
            } while (!board_.IsMoveValid(move.Item1, move.Item2));

            possibilities_.Remove(move);
            return Tuple.Create(move.Item1, move.Item2);
        }
        public bool RemovePossibility(Tuple<int, int> _possibility)
        {
            return possibilities_.Remove(_possibility);
        }
    }
}