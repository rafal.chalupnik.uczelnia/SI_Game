﻿using System;
using System.Linq;
using Logic.Algorithms.Helpers;

namespace Logic.Algorithms
{
    public class AlphaBetaAlgorithm : Algorithm<AlphaBetaAlgorithm>
    {
        private static int ExecuteAlphaBeta(bool _isPlayerOne, Board _board, Node _node, int _depth, 
            int _alpha = int.MinValue, int _beta = int.MaxValue)
        {
            if (_depth == 0 || _board.IsGameOver)
            {
                _node.HeuristicValue = Heuristic.GetBoardHeuristicValue(_board);
                return _node.HeuristicValue;
            }

            if (_isPlayerOne)
            {
                var boardSize = _board.Size;
                for (var r = 0; r < boardSize; r++)
                {
                    for (var c = 0; c < boardSize; c++)
                    {
                        if (_board.IsMoveValid(r, c))
                        {
                            var childNode = new Node(Tuple.Create(r, c));
                            var childBoard = _board.Clone() as Board;
                            childBoard.Move(r, c);
                            _node.Children.Add(childNode);
                            var childValue = ExecuteAlphaBeta(false, childBoard, childNode, _depth - 1, _alpha, _beta);
                            _node.HeuristicValue = Math.Max(_alpha, childValue);
                            _alpha = _node.HeuristicValue;
                            if (_alpha >= _beta)
                                return _alpha;
                        }
                    }
                }
            }
            else
            {
                var boardSize = _board.Size;
                for (var r = 0; r < boardSize; r++)
                {
                    for (var c = 0; c < boardSize; c++)
                    {
                        if (_board.IsMoveValid(r, c))
                        {
                            var childNode = new Node(Tuple.Create(r, c));
                            var childBoard = _board.Clone() as Board;
                            childBoard.Move(r, c);
                            _node.Children.Add(childNode);
                            var childValue = ExecuteAlphaBeta(true, childBoard, childNode, _depth - 1, _alpha, _beta);
                            _node.HeuristicValue = Math.Min(_beta, childValue);
                            _beta = _node.HeuristicValue;
                            if (_alpha >= _beta)
                                return _beta;
                        }
                    }
                }
            }

            return _node.HeuristicValue;
        }

        private readonly Random random_ = new Random();

        private readonly Board board_;
        private readonly int depth_;
        private readonly bool isPlayerOne_;

        private AlphaBetaAlgorithm(Board _board, bool _isPlayerOne, int _depth)
        {
            board_ = _board;
            depth_ = _depth;
            isPlayerOne_ = _isPlayerOne;
        }

        public override Tuple<int, int> GetMove()
        {
            var root = new Node();
            var bestScore = ExecuteAlphaBeta(isPlayerOne_, board_, root, depth_);

            if (root.Children.Count == 1)
                return root.Children.First().Move;

            var bestSolutions = root.Children.Where(_child => _child.HeuristicValue == bestScore).ToList();
            var bestSolution = bestSolutions[random_.Next(bestSolutions.Count)];

            return bestSolution.Move;
        }
    }
}