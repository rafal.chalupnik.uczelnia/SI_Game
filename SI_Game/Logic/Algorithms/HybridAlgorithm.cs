﻿using System;

namespace Logic.Algorithms
{
    public class HybridAlgorithm : Algorithm<HybridAlgorithm>
    {
        private readonly AlphaBetaAlgorithm alphaBetaLinqAlgorithm_;
        private readonly Board board_;
        private readonly GreedyAlgorithm greedyAlgorithm_;

        private HybridAlgorithm(Board _board, bool _isPlayerOne, int _depth)
        {
            board_ = _board;

            alphaBetaLinqAlgorithm_ = Algorithm<AlphaBetaAlgorithm>.Create(_board, _isPlayerOne, _depth);
            greedyAlgorithm_ = Algorithm<GreedyAlgorithm>.Create(_board, _isPlayerOne, _depth);
        }

        public override Tuple<int, int> GetMove()
        {
            return board_.MovesMade <= board_.SquaredSize / 2
                ? greedyAlgorithm_.GetMove() 
                : alphaBetaLinqAlgorithm_.GetMove();
        }
    }
}