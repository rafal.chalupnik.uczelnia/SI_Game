﻿using System;
using System.Linq;
using Logic.Algorithms.Helpers;

namespace Logic.Algorithms
{
    public class MinimaxAlgorithm : Algorithm<MinimaxAlgorithm>
    {
        private static void BuildTree(Board _board, Node _node, int _depth)
        {
            if (_depth == 0 || _board.IsGameOver)
            {
                _node.HeuristicValue = Heuristic.GetBoardHeuristicValue(_board);
                return;
            }

            if (_node.Children.Count != 0 && _depth != 0)
            {
                var childBoard = _board.Clone() as Board;
                childBoard.Move(_node.Move.Item1, _node.Move.Item2);

                foreach (var child in _node.Children)
                    BuildTree(childBoard, child, _depth - 1);
            }
            else
            {
                var boardSize = _board.Size;

                for (var r = 0; r < boardSize; r++)
                {
                    for (var c = 0; c < boardSize; c++)
                    {
                        if (_board.IsMoveValid(r, c))
                        {
                            var childNode =
                                new Node(Tuple.Create(r, c)) { IsPlayerOne = !_node.IsPlayerOne };
                            var childBoard = _board.Clone() as Board;
                            childBoard.Move(r, c);
                            _node.Children.Add(childNode);
                            BuildTree(childBoard, childNode, _depth - 1);
                        }
                    }
                }
            }

            var childrenHeuristicValues = _node.Children.Select(_n => _n.HeuristicValue);

            // Gracz pierwszy maksymalizuje, drugi minimalizuje
            _node.HeuristicValue = _node.IsPlayerOne ? childrenHeuristicValues.Max() : childrenHeuristicValues.Min();
        }

        private readonly Random random_ = new Random();

        private readonly Board board_;
        private readonly int depth_;
        private readonly bool isPlayerOne_;

        private MinimaxAlgorithm(Board _board, bool _isPlayerOne, int _depth)
        {
            board_ = _board;
            depth_ = _depth;
            isPlayerOne_ = _isPlayerOne;
        }
        
        public override Tuple<int, int> GetMove()
        {
            var root = new Node {IsPlayerOne = isPlayerOne_};
            BuildTree(board_, root, depth_);

            var bestSolutions = root.Children.Where(_child => _child.HeuristicValue == root.HeuristicValue).ToList();
            var bestSolution = bestSolutions[random_.Next(bestSolutions.Count)];

            return bestSolution.Move;
        }
    }
}