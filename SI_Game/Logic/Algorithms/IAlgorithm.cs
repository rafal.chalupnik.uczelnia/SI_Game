﻿using System;
using System.Reflection;

namespace Logic.Algorithms
{
    public interface IAlgorithm
    {
        Tuple<int, int> GetMove();
    }

    public abstract class Algorithm<T> : IAlgorithm where T : class, IAlgorithm
    {
        public static T Create(Board _board, bool _isPlayerOne = false, int _depth = 1)
        {
            return _board != null && _depth >= 1
                ? Activator.CreateInstance(
                    type: typeof(T),
                    bindingAttr: BindingFlags.NonPublic | BindingFlags.Instance, 
                    binder: null, 
                    args: new object[] {_board, _isPlayerOne, _depth}, 
                    culture: null) as T
                : null;
        }

        public abstract Tuple<int, int> GetMove();
    }
}