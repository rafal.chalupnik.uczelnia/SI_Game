﻿using Logic;
using Logic.Algorithms;
using System;

namespace Stratego_CLI
{
    public static class Program
    {
        public static void Main(string[] _args)
        {
            var board = new Board(6);
            var randomAlgorithm = Algorithm<AlphaBetaAlgorithm>.Create(board, true, 3);
            Console.WriteLine(randomAlgorithm.GetMove());
            Console.ReadLine();
        }
    }
}