﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Logic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Stratego_GUI
{
    /// <summary>
    /// Wspólne
    /// </summary>
    public partial class GameWindow
    {
        public int BoardSize { get; set; }
        public List<Rectangle> Fields { get; set; }
        public Brush FreeFieldBrush { get; } = new SolidColorBrush(Color.FromRgb(128, 128, 128));
        public Game Game { get; set; }
        public bool IsClickable { get; set; } = true;
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
        public DateTime StartTime { get; set; }
        public Brush TakenFieldBrush { get; } = new SolidColorBrush(Color.FromRgb(0, 0, 0));
        public System.Timers.Timer Timer { get; set; }

        public GameWindow()
        {
            InitializeComponent();

            playerAdditionalValueLabels_ = new[]
            {
                Player1AdditionalValueLabel,
                Player2AdditionalValueLabel
            };
            playerAdditionalValueTextBoxes_ = new[]
            {
                Player1AdditionalValueTextBox,
                Player2AdditionalValueTextBox
            };
            playerTypesComboBoxes_ = new[]
            {
                Player1TypeComboBox,
                Player2TypeComboBox
            };
        }
    }

    /// <summary>
    /// Lewy panel
    /// </summary>
    public partial class GameWindow
    {
        private readonly Label[] playerAdditionalValueLabels_;
        private readonly TextBox[] playerAdditionalValueTextBoxes_;
        private readonly ComboBox[] playerTypesComboBoxes_;

        private bool PlayerNeedsDepth(Player _player)
        {
            return _player != Player.Greedy
                   && _player != Player.Human
                   && _player != Player.Random;
        }
        private void PlayerTypeChanged(object _sender, SelectionChangedEventArgs _eventArgs)
        {
            UpdateLeftPanel(_sender == Player1TypeComboBox ? 0 : 1);
        }
        private void StartButtonClick(object _sender, RoutedEventArgs _eventArgs)
        {
            if (ValidateGameData())
            {
                BoardSize = int.Parse(BoardSizeTextBox.Text);
                InitializeBoard();

                Player1 = (Player) playerTypesComboBoxes_[0].SelectedIndex;
                Player2 = (Player) playerTypesComboBoxes_[1].SelectedIndex;

                Game = Game.Create(BoardSize);

                Game.SetComputerPlayer(Player1, 0,
                    PlayerNeedsDepth(Player1) ? int.Parse(playerAdditionalValueTextBoxes_[0].Text) : 1);
                Game.SetComputerPlayer(Player2, 1,
                    PlayerNeedsDepth(Player2) ? int.Parse(playerAdditionalValueTextBoxes_[1].Text) : 1);

                TimeLabel.Content = "00:00";
                UpdateRightPanel();

                Timer = new System.Timers.Timer(1000);
                Timer.Elapsed += (_o, _args) =>
                {
                    var ellapsed = DateTime.Now - StartTime;
                    Dispatcher.Invoke(() => TimeLabel.Content = ellapsed.ToString(@"mm\:ss"));

                };
                StartTime = DateTime.Now;
                Timer.Start();

                Task.Run((Action)ProcessComputerMove);
            }
        }
        private void UpdateLeftPanel(int _player)
        {
            if (playerTypesComboBoxes_ == null)
                return;

            var playerType = (Player) playerTypesComboBoxes_[_player].SelectedIndex;
            var additionalValueVisible = playerType != Player.Random && playerType != Player.Greedy;

            var visibility = additionalValueVisible ? Visibility.Visible : Visibility.Hidden;
            playerAdditionalValueLabels_[_player].Visibility = visibility;
            playerAdditionalValueTextBoxes_[_player].Visibility = visibility;

            if (additionalValueVisible)
            {
                playerAdditionalValueLabels_[_player].Content = playerType == Player.Human
                    ? "Imię:"
                    : "Głębokość";
            }
        }
        private bool ValidateGameData()
        {
            for (var i = 0; i < playerAdditionalValueLabels_.Length; i++)
            {
                if (playerAdditionalValueLabels_[i].Visibility == Visibility.Visible)
                {
                    var content = playerAdditionalValueTextBoxes_[i].Text;

                    if (string.IsNullOrEmpty(content))
                        return false;

                    if (playerAdditionalValueLabels_[i].Content.ToString() == "Głębokość:"
                        && !int.TryParse(content, out var depth))
                        return false;
                }
            }

            return int.TryParse(BoardSizeTextBox.Text, out var boardSize) && boardSize > 1;
        }
    }

    /// <summary>
    /// Środkowy panel
    /// </summary>
    public partial class GameWindow
    {
        private void FieldClicked(object _sender, MouseButtonEventArgs _mouseButtonEventArgs)
        {
            if (IsClickable)
            {
                var rectangle = _sender as Rectangle;
                var row = Grid.GetRow(rectangle);
                var column = Grid.GetColumn(rectangle);
                var move = Tuple.Create(row, column);

                if (Game.HumanMove(move))
                {
                    rectangle.Fill = TakenFieldBrush;
                    UpdateRightPanel();
                    Task.Run((Action) ProcessComputerMove);
                }
            }
        }

        private void ProcessComputerMove()
        {
            // Zablokuj człowieka
            IsClickable = false;

            while (!Game.IsGameOver && !Game.IsNowHumanMove())
            {
                var move = Game.GetComputerMove();
                var field = Fields[move.Item1 * BoardSize + move.Item2];
                Dispatcher.Invoke(() => field.Fill = TakenFieldBrush);
                Dispatcher.Invoke(UpdateRightPanel);
                //Thread.Sleep(500);
            }

            // Odblokuj człowieka
            IsClickable = true;
        }

        public void InitializeBoard()
        {
            BoardGrid.RowDefinitions.Clear();
            BoardGrid.ColumnDefinitions.Clear();
            for (var i = 0; i < BoardSize; i++)
            {
                BoardGrid.RowDefinitions.Add(new RowDefinition());
                BoardGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            var squaredSize = BoardSize * BoardSize;

            Fields = new List<Rectangle>();
            BoardGrid.Children.Clear();
            for (var i = 0; i < squaredSize; i++)
            {
                var row = i / BoardSize;
                var column = i % BoardSize;
                var rectangle = new Rectangle()
                {
                    Fill = FreeFieldBrush,
                    StrokeThickness = 0.5,
                    Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0))
                };
                rectangle.MouseLeftButtonDown += FieldClicked;
                Grid.SetRow(rectangle, row);
                Grid.SetColumn(rectangle, column);
                BoardGrid.Children.Add(rectangle);
                Fields.Add(rectangle);
            }
        }
    }

    /// <summary>
    /// Prawy panel
    /// </summary>
    public partial class GameWindow
    {
        private string GetPlayerIdentifier(int _player)
        {
            var player = _player == 0 ? Player1 : Player2;

            return player == Player.Human 
                ? playerAdditionalValueTextBoxes_[_player].Text 
                : player.ToString();
        }

        public void UpdateRightPanel()
        {
            var scores = Game.Scores;
            Player1ScoreLabel.Content = scores[0];
            Player2ScoreLabel.Content = scores[1];

            if (Game.IsGameOver)
            {
                Timer.Stop();

                ActualPlayerLabel.Content = "Koniec gry";
                var message = "";
                if (scores[0] == scores[1])
                    message = "Remis!";
                else
                {
                    var winner = scores[0] > scores[1] ? 0 : 1;
                    message = $"Wygrał {GetPlayerIdentifier(winner)}";
                }

                MessageBox.Show(message);
            }
            else
            {
                var actualPlayer = Game.ActualPlayer;
                ActualPlayerLabel.Content = GetPlayerIdentifier(actualPlayer);
            }
        }
    }
}