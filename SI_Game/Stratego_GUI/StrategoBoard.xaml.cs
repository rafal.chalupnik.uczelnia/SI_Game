﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Logic;

namespace Stratego_GUI
{
    public partial class StrategoBoard : UserControl
    {
        private void FieldClicked(object _sender, MouseButtonEventArgs _mouseButtonEventArgs)
        {
            if (IsClickable && PlayerMoved != null)
            {
                var rectangle = _sender as Rectangle;
                var row = Grid.GetRow(rectangle);
                var column = Grid.GetColumn(rectangle);
                GameBoard.Move(row, column);
                UpdateBoard();
                PlayerMoved.Invoke(Tuple.Create(row, column), null);
            }
        }

        private void SetGrid()
        {
            if (GameBoard == null)
                return;

            var boardSize = GameBoard.Size;

            BoardGrid.RowDefinitions.Clear();
            BoardGrid.ColumnDefinitions.Clear();
            for (var i = 0; i < boardSize; i++)
            {
                BoardGrid.RowDefinitions.Add(new RowDefinition());
                BoardGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            UpdateBoard();
        }
        
        public static readonly DependencyProperty FreeFieldBrushProperty = 
            DependencyProperty.Register("FreeFieldBrush", typeof(Brush), typeof(StrategoBoard), 
                new PropertyMetadata(new SolidColorBrush(Color.FromRgb(66, 66, 66))));
        public static readonly DependencyProperty GameBoardProperty =
            DependencyProperty.Register("GameBoard", typeof(Board), typeof(StrategoBoard), new PropertyMetadata(null));
        public static readonly DependencyProperty IsClickableProperty =
            DependencyProperty.Register("IsClickable", typeof(bool), typeof(StrategoBoard), new PropertyMetadata(true));
        public static readonly DependencyProperty TakenFieldBrushProperty =
            DependencyProperty.Register("TakenFieldBrush", typeof(Brush), typeof(StrategoBoard), 
                new PropertyMetadata(new SolidColorBrush(Color.FromRgb(33, 33, 33))));
        
        public Brush FreeFieldBrush
        {
            get => (Brush) GetValue(FreeFieldBrushProperty);
            set => SetValue(FreeFieldBrushProperty, value);
        }
        public Board GameBoard
        {
            get => (Board) GetValue(GameBoardProperty);
            set
            {
                SetValue(GameBoardProperty, value);
                SetGrid();
            }
        }
        public bool IsClickable
        {
            get => (bool) GetValue(IsClickableProperty);
            set => SetValue(IsClickableProperty, value);
        }
        public event EventHandler PlayerMoved;
        public Brush TakenFieldBrush
        {
            get => (Brush) GetValue(TakenFieldBrushProperty);
            set => SetValue(TakenFieldBrushProperty, value);
        }

        public StrategoBoard()
        {
            InitializeComponent();
            SetGrid();
        }

        public void UpdateBoard()
        {
            var boardSize = GameBoard.Size;
            var squaredSize = GameBoard.SquaredSize;

            BoardGrid.Children.Clear();
            for (var i = 0; i < squaredSize; i++)
            {
                var row = i / boardSize;
                var column = i % boardSize;
                var rectangle = new Rectangle()
                {
                    Fill = GameBoard[row, column] == ' ' ? FreeFieldBrush : TakenFieldBrush,
                    StrokeThickness = 0.5,
                    Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0))
                };
                rectangle.MouseLeftButtonDown += FieldClicked;
                Grid.SetRow(rectangle, row);
                Grid.SetColumn(rectangle, column);
                BoardGrid.Children.Add(rectangle);
            }
        }
    }
}